## Churn prediction Project
The objective is to predict churn in the context of a courier's logistic for a retail company.

## Project Planning - Prototype Fase
![alt text](data/images/planning.png)

## advances

| Step  | status | observations |
| --- | --- | --- |
|1  | Done| Exploring CLT_data.csv |
|1  | Done | Exploring CHdata.csv |
|2  | WorkingOnIt | Baselina dataset generation |